import { v4 as uuidv4 } from 'uuid';

export enum CurrencyEnum {
  USD = 'USD',
  UAH = 'UAH',
}


export interface ICard {
  addTransaction(transaction: Transaction): string;
  addTransaction(amount: number, currency: CurrencyEnum): string;
  getTransaction(id: string): Transaction | undefined;
  getBalance(currency: CurrencyEnum): number;
}


export class Transaction {
  id: string;
  amount: number;
  currency: CurrencyEnum;

  constructor(amount: number, currency: CurrencyEnum) {
    this.id = uuidv4();
    this.amount = amount;
    this.currency = currency;
  }
}

export class Card implements ICard {
  transactions: Transaction[] = [];

  addTransaction(transactionOrAmount: Transaction | number, currency?: CurrencyEnum): string {
    if (typeof transactionOrAmount === 'number' && currency) {
      const newTransaction = new Transaction(transactionOrAmount, currency);
      this.transactions.push(newTransaction);
      return newTransaction.id;
    } else {
      const typedTransaction = transactionOrAmount as Transaction;
      this.transactions.push(typedTransaction);
      return typedTransaction.id;
    }
  }

  getTransaction(id: string): Transaction | undefined {
    return this.transactions.find((transaction) => transaction.id === id);
  }

  getBalance(currency: CurrencyEnum): number {
    return this.transactions
      .filter((transaction) => transaction.currency === currency)
      .reduce((total, transaction) => total + transaction.amount, 0);
  }
}

export class BonusCard extends Card {
  addTransaction(transactionOrAmount: number | Transaction, currency?: CurrencyEnum | undefined): string {
    let newTransaction: Transaction;
    if (typeof transactionOrAmount === 'number' && currency) {
      newTransaction = new Transaction(transactionOrAmount, currency)
    } else {
      newTransaction = transactionOrAmount as Transaction
    }
    const bonusAmount = newTransaction.amount*0.1;
    const bonusTransaction = new Transaction(bonusAmount, newTransaction.currency);
    this.transactions.push(bonusTransaction);
    return super.addTransaction(newTransaction);
  }
}

export class Pocket {
  cards: Record<string, ICard> = {};

  addCard(name: string, card: ICard): void {
    this.cards[name] = card;
  }

  removeCard(name: string): void {
    delete this.cards[name];
  }

  getCard(name: string): ICard | undefined {
    return this.cards[name];
  }

  getTotalAmount(currency: CurrencyEnum): number {
    let totalAmount = 0;
    for (const cardName in this.cards) {
      if (Object.prototype.hasOwnProperty.call(this.cards, cardName)) {
        const card = this.cards[cardName];
        totalAmount += card.getBalance(currency);
      }
    }
    return totalAmount;
  }
}

const pocket = new Pocket();

const card = new Card();
const bonusCard = new BonusCard();

pocket.addCard('regular', card);
pocket.addCard('bonus', bonusCard);

const transactionId1 = pocket.getCard('regular')?.addTransaction(100, CurrencyEnum.USD);
console.log(pocket.getCard('regular')?.getTransaction(transactionId1!));

const transactionId2 = pocket.getCard('bonus')?.addTransaction(200, CurrencyEnum.UAH);
console.log(pocket.getCard('bonus')?.getTransaction(transactionId2!));

console.log(pocket.getTotalAmount(CurrencyEnum.USD));
console.log(pocket.getTotalAmount(CurrencyEnum.UAH));
