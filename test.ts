import { v4 as uuidv4 } from 'uuid';
import { Pocket, ICard, Card, CurrencyEnum, Transaction } from './app'; 

describe('Pocket', () => {
    let pocket: Pocket;
    let card1: ICard;
    let card2: ICard;

    beforeEach(() => {
        pocket = new Pocket();
        card1 = new Card();
        card2 = new Card();
        card1.addTransaction(new Transaction(100, CurrencyEnum.USD));
        card1.addTransaction(new Transaction(200, CurrencyEnum.UAH));
        card2.addTransaction(new Transaction(50, CurrencyEnum.USD));
        card2.addTransaction(new Transaction(150, CurrencyEnum.UAH));
        pocket.addCard('regular', card1);
        pocket.addCard('bonus', card2);
    });

    test('Add and get card', () => {
        const newCard: ICard = new Card();
        pocket.addCard('test', newCard);
        expect(pocket.getCard('test')).toBe(newCard);
    });

    test('Remove card', () => {
        pocket.removeCard('regular');
        expect(pocket.getCard('regular')).toBeUndefined();
    });

    test('Get total amount in USD', () => {
        expect(pocket.getTotalAmount(CurrencyEnum.USD)).toBe(150); 
    });

    test('Get total amount in UAH', () => {
        expect(pocket.getTotalAmount(CurrencyEnum.UAH)).toBe(350); 
    });
});